import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';

import Home from './components/Home';
import ViewCharts from './components/ViewCharts';

const Routes = () => {
  return (
    <div>
      <Switch>
        <Route 
          exact         
          path={'/upload'}
          component={Home}
        />
        <Route 
          exact         
          path={'/view'}
          component={ViewCharts}
        />
        <Redirect from="*" to="/view" />
      </Switch>
    </div>
  );
}


export default Routes;
