import React, { Component } from 'react';
import Sidebar from '../Sidebar';
import {
    Radio, FormGroup, FormControlLabel, withStyles
} from '@material-ui/core';

import { API } from 'aws-amplify';
import { BarChart } from 'react-easy-chart';

const styles = theme => ({
    root: {
        flexGrow: 1,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
    },
    paper: {
        position: 'absolute',
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
        overflow: 'auto',
        position: 'relative',
        display: 'flex',
        width: '100%',
        flexDirection: 'column'
    },
    categoryChip: {
        margin: '0 10px'
    },
});

class ViewCharts extends Component {
    constructor() {
        super()

        this.state = {
            files: [],
            selectedFile: {},
            defaultData: [],
            yDomainRange: 1
        }
    }

    async componentDidMount() {
        try {
            let { yDomainRange } = this.state;
            const files = await API.get('Charts', '/uploads');
            const selectedFile = files[0] || {};
            const defaultData = selectedFile.data ? selectedFile.data.map(x => {
                if (+x[selectedFile.y_axis] > yDomainRange) {
                    yDomainRange = +x[selectedFile.y_axis]
                }
                return {
                    x: x[selectedFile.x_axis],
                    y: +x[selectedFile.y_axis]
                }
            }) : [];

            this.setState({ files, selectedFile, defaultData, yDomainRange });
        } catch (e) {
            console.log(e);
        }
    }

    handleChange = (e) => {
        let yDomainRange = 0;
        const selectedFile = this.state.files.filter(x => x.file_path === e.target.value)[0];
        const defaultData = selectedFile.data.map(x => {
            if (+x[selectedFile.y_axis] > yDomainRange) {
                yDomainRange = +x[selectedFile.y_axis]
            }
            return {
                x: x[selectedFile.x_axis],
                y: +x[selectedFile.y_axis]
            }
        })
        this.setState({ selectedFile, defaultData, yDomainRange })
    }

    render() {
        const { classes } = this.props;
        const props = Object.assign({}, this.props);
        delete props.classes;

        const { files, selectedFile, defaultData, yDomainRange } = this.state;
        console.log(yDomainRange);

        return (
            <div className={classes.root}>
                <Sidebar {...props} />
                <div className={classes.content}>
                    <FormGroup row>
                        {
                            files.map((file, index) => (<FormControlLabel
                                key={index}
                                control={
                                    <Radio
                                        checked={selectedFile.file_path === file.file_path}
                                        onChange={this.handleChange}
                                        value={file.file_path}
                                        name={file.file_path}
                                    />
                                }
                                label={file.file_path}
                            />))
                        }
                    </FormGroup>

                    <BarChart
                        axisLabels={{ x: selectedFile.x_axis, y: selectedFile.y_axis }}
                        axes
                        colorBars
                        xTickNumber={5}
                        yTickNumber={yDomainRange > 10 ? yDomainRange / 2 : yDomainRange}
                        yDomainRange={[0, yDomainRange ]}
                        height={400}
                        width={800}
                        data={defaultData}
                    />
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(ViewCharts);

