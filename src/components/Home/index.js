import React, { Component } from 'react';
import { withStyles, TextField, Button } from '@material-ui/core';
import Sidebar from '../Sidebar';
import { API, Storage } from 'aws-amplify';

const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
    flexDirection: 'column'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%'
  },
});

class Home extends Component {
  constructor() {
    super()
    this.state = {
      input: {
        xAxis: '',
        yAxis: ''
      },
      inProgess: false,
      file: ''
    }
  }

  uploadFile = (evt) => {
    const file = evt.target.files[0];
    const name = file.name;

    Storage.put(name, file).then(() => {
      this.setState({ file: name });
    })
  }


  handleChange = e => {
    const { input } = this.state;
    input[e.target.name] = e.target.value;
    this.setState({ input });
  }

  save = async () => {
    const { input: { xAxis: x_axis, yAxis: y_axis }, file: file_path } = this.state;
    this.setState({ inProgess: true });
    try {
      await API.post('Charts', '/uploads', {
        body: {
          x_axis, y_axis, file_path
        }
      });
    } catch (e) {
      console.log(e)
    }

    this.setState({ inProgess: false })
  }

  render() {
    const { classes } = this.props;
    const props = Object.assign({}, this.props);
    delete props.classes;

    return (
      <div className={classes.root}>
        <Sidebar {...props} />
        <div className={classes.content}>

          <div>
            <TextField
              label="Upload"
              className={classes.textField}
              name="file"
              onChange={this.uploadFile}
              margin="normal"
              variant="outlined"
              type="file"
              accept=".csv"
              fullWidth
            />

            <TextField
              label="X-Axis"
              className={classes.textField}
              value={this.state.input.xAxis}
              name="xAxis"
              onChange={this.handleChange}
              margin="normal"
              variant="outlined"
              fullWidth
            />

            <TextField
              label="Y-Axis"
              className={classes.textField}
              value={this.state.input.yAxis}
              name="yAxis"
              onChange={this.handleChange}
              margin="normal"
              variant="outlined"
              fullWidth
            />
          </div>
          <Button color="primary" variant="contained" onClick={this.save}>Save</Button>
          {
            (this.state.inProgess) ? <p>Saving....</p> : null
          }
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Home);

