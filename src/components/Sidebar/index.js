import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import { Drawer, Divider, List, ListItem, ListItemText } from '@material-ui/core';

const styles = theme => ({
  root: {
    flexGrow: 1,
    border: '1px solid #eee',
  },
  drawerPaper: {
    position: 'relative',
    height: window.innerHeight - 67,
    backgroundColor: theme.palette.background.default,

  },
  toolbar: theme.mixins.toolbar,
  active: {
    backgroundColor: '#ff9900',
    cursor: 'pointer',
    '& span': {
      color: '#fff'
    },
    '& svg': {
      color: '#fff'
    }
  },
  menu: {
    cursor: 'pointer',
  },
  activeText: {
    // display: 'none',
    backgroundColor: '#ff9900',
    cursor: 'pointer',
    '& span': {
      color: '#fff'
    },
    '& svg': {
      color: '#fff'
    }
  },
  menuText: {
    // display: 'none',
    cursor: 'pointer',
  },
  menuIcon: {
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  }
});

class Sidebar extends Component {
  gotTo = path => {
    this.props.history.push(path)
  }

  render() {
    const { classes, match: { path } } = this.props;
    return (
      <div className={classes.root}>
        <Drawer
          variant="permanent"
          classes={{
            paper: classes.drawerPaper,
          }}
          anchor="left"
        >
          <List>

            <ListItem onClick={e => this.gotTo('/upload')} className={path === '/upload' ? classes.active : classes.menu}>
              <ListItemText className={path === '/upload' ? classes.activeText : classes.menuText}>Upload</ListItemText>
            </ListItem>

            <ListItem onClick={e => this.gotTo('/View')} className={path === '/view' ? classes.active : classes.menu}>
              <ListItemText className={path === '/View' ? classes.activeText : classes.menuText}>View Charts</ListItemText>
            </ListItem>
          </List>
        </Drawer>
      </div>
    );
  }
}

export default withStyles(styles)(Sidebar);

