import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Amplify, { Auth } from 'aws-amplify';
import { withAuthenticator } from 'aws-amplify-react';

import './App.css';
import Routes from './router';
import awsmobile from './aws-exports';

Amplify.configure(awsmobile);
Amplify.configure({
  API: {
    endpoints: [
        {
            name: "Charts",
            endpoint: "https://ucjtmzqn1h.execute-api.us-east-1.amazonaws.com/dev",
            custom_header: async () => {
              const { idToken: { jwtToken }} = await Auth.currentSession();
              return { Authorization: jwtToken }
            }
        }
    ]
  } 
});

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Routes />
      </BrowserRouter>
    );
  }
}

const signUpConfig = {
  hideAllDefaults: true,
  signUpFields: [
    {
      label: 'Username',
      key: 'username',
      required: true,
      placeholder: 'Username',
      displayOrder: 1
    },
    {
      label: 'Email',
      key: 'email',
      required: true,
      placeholder: 'Email',
      type: 'email',
      displayOrder: 2
    },
    {
      label: 'Password (Min length: 6)',
      key: 'password',
      required: true,
      placeholder: 'Password',
      type: 'password',
      displayOrder: 3
    }
  ]
};

export default withAuthenticator(App, {
  includeGreetings: true,
  signUpConfig
});
